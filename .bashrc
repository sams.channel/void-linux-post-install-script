# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias nf="neofetch"
alias reset="clear"
alias cat="bat"
alias ls="exa"
alias ll="exa -lag"
alias lsl="exa -lg"
alias la="exa -a"

alias update="sudo xbps-install -Syu && flatpak update"

PS1='[\u@\h \W]\$ '

export XDG_RUNTIME_DIR=~/.sway
export PATH=$PATH:~/.cargo/bin

alias vms="sudo virsh list --all"

footssh()
{
    if [[ $TERM = "foot" ]]; then
        TERM=linux ssh $@
    else
        ssh $@
    fi
}

alacrittyssh()
{
    if [[ $TERM = "alacritty" ]]; then
	TERM=linux ssh $@
    else
	ssh $@
    fi
}

alias ssh=alacrittyssh

eval `keychain --agents ssh --eval id_rsa`
