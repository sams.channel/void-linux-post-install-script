#!/bin/bash
# void-linux-post-install.sh
# A post install script to install Sway, ly, LXDE and other
# compilers and applications on Void Linux (glibc).
# Please run this script as sudo/doas or under root user.

echo "Starting Void Linux post install..."
read -p "Enter username to set up: " setup_user
useradd $setup_user
passwd $setup_user

usermod -aG wheel $setup_user
usermod -aG tty $setup_user
usermod -aG floppy $setup_user
usermod -aG audio $setup_user
usermod -aG video $setup_user
usermod -aG cdrom $setup_user
usermod -aG optical $setup_user
usermod -aG kvm $setup_user
usermod -aG xbuilder $setup_user

mkdir -p /home/$setup_user/.sway
mkdir -p /home/$setup_user/.config/sway
mkdir -p /home/$setup_user/.config/alacritty
mkdir -p /home/$setup_user/.ssh

chown -R $setup_user:$setup_user /home/$setup_user/

cp alacritty.yml /home/$setup_user/.config/alacritty/
cp .bashrc /home/$setup_user/
cp .vimrc /home/$setup_user/
cp void_ly_patch.diff /home/$setup_user/
cp .xinitrc /home/$setup_user/

chown $setup_user:$setup_user /home/$setup_user/.config/alacritty/alacritty.yml
chown $setup_user:$setup_user /home/$setup_user/.bashrc
chown $setup_user:$setup_user /home/$setup_user/.vimrc
chown $setup_user:$setup_user /home/$setup_user/void_ly_patch.diff
chown $setup_user:$setup_user /home/$setup_user/.xinitrc

ln -s /home/$setup_user/.config/alacritty/alacritty.yml /home/$setup_user/alacritty.yml

xbps-pkgb hold -m linux6.1
xbps-install -Syv xbps
xbps-install -Syu
xbps-install -Syv dbus-elogind
xbps-install -Syv polkit
xbps-install -Syv elogind
xbps-install -Syv mesa-dri
xbps-install -Syv xorg-fonts
xbps-install -Syv gvfs
xbps-install -Syv foot alacritty
xbps-install -Syv gvfs
xbps-install -Syv dmenu
xbps-install -Syv sway
xbps-install -Syv xorg lxde
xbps-install -Syv alsa-utils
xbps-install -Syv pulseaudio
xpbs-install -Syv pipewire
xbps-install -Syv p7zip xarchiver

ln -s /etc/sv/dbus /var/service/
ln -s /etc/sv/polkitd /var/service/
ln -s /etc/sv/alsa /var/service/

xbps-install -Syv gcc make upx
xbps-install -Syv rust cargo dmd ldc
xbps-install -Syv git curl wget
xbps-install -Syv patch
xbps-install -Syv neofetch vim nano
xbps-install -Syv firefox
xbps-install -Syv filezilla
xbps-install -Syv keychain keepassxc
xbps-install -Syv docker
xbps-install -Syv qemu virt-manager
xbps-install -Syv pam-devel libxcb-devel

ln -s /etc/sv/docker /var/service/
usermod -aG docker $setup_user

ln -s /etc/sv/libvirtd /var/service/
ln -s /etc/sv/virtlogd /var/service/

mkdir -p /home/$setup_user/BuildFromSrc
mkdir -p /home/$setup_user/Projects

mkdir -p /home/$setup_user/.config/sway
cp /etc/sway/config /home/$setup_user/.config/sway/

cd /home/$setup_user/BuildFromSrc
git clone --recurse-submodules https://github.com/fairyglade/ly.git
cd ly/src
patch config.c < /home/$setup_user/void_ly_patch.diff
rm /home/$setup_user/void_ly_patch.diff
cd ..
make
make install installrunit
rm /var/service/agetty-tty2
ln -s /etc/sv/ly /var/service/

echo "Finished. Now reboot."
