set nu
call plug#begin()
Plug 'editorconfig/editorconfig-vim'
Plug 'https://github.com/airblade/vim-gitgutter.git'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'https://github.com/preservim/nerdtree.git'
Plug 'cespare/vim-toml'
Plug 'Tetralux/odin.vim'
call plug#end()
let g:airline_theme='deus'
set wrap

nmap ! :NERDTreeToggle
